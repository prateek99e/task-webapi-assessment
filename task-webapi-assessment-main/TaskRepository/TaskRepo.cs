﻿using System.Threading.Tasks;
using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.Models;



namespace TaskApi.TaskRepository
{
    public class TaskRepo : ITask
    {
        TaskDbContext _context;
        public TaskRepo(TaskDbContext context)
        {
            _context = context;
        }
        public void AddTask(Task2 task)
        {
            _context.Tasks.Add(task);
            _context.SaveChanges();
        }



        public void DeleteTask(int id)
        {
            Task2 t = _context.Tasks.FirstOrDefault(x => x.Id == id);
            if (t != null)
            {
                _context.Tasks.Remove(t);
                _context.SaveChanges();
            }
        }



        public void EditTask(int id, Task2 task)
        {
            Task2 t = _context.Tasks.FirstOrDefault(x => x.Id == id);
            if (t != null)
            {
                t.CreatedBy= task.CreatedBy;
                t.TaskName= task.TaskName;
                t.CreatedBy=task.CreatedBy;
                t.TaskDescription= task.TaskDescription;

                _context.Tasks.Update(t);
                _context.SaveChanges();
            }
        }


        public Task2 GetTaskById(int id)
        {
            Task2 task = _context.Tasks.FirstOrDefault(x => x.Id == id);
            return task;
        }
        public List<Task2> GetTasks()
        {
            return _context.Tasks.ToList();



        }
        public List<SubTask> GetSubTasksByTaskId(int id) {
            return _context.SubTasks.Where(x => x.Task2Id == id).ToList();
        }
    }
}